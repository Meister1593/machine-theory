use std::env;
use std::collections::HashMap;

// a -> bdbd/dbdb
// a+ -> bdb/dbd
// a- -> bbd/dbb

fn main() {
    let available_symbols = vec!['a', '-', '+'];
    let mut association_map: HashMap<String, String> = HashMap::new();
    association_map.insert(String::from("a"), String::from("bdbd"));
    association_map.insert(String::from("a+"), String::from("bdb"));
    association_map.insert(String::from("a-"), String::from("bbd"));

    let arguments: Vec<String> = env::args().collect();
    if arguments.len() <= 1 {
        println!("No arguments passed");
        return;
    }

    let mut prepared_sentence: String = arguments[1..].concat();
    prepared_sentence.retain(|char| !char.is_whitespace());
    println!("{:?} - args", prepared_sentence);

    let result = convert(&available_symbols, &association_map, &prepared_sentence);

    println!("{:?} -> result", result);
}

fn convert(available_symbols: &Vec<char>,
           association_map: &HashMap<String, String>,
           sentence: &String) -> String {
    let mut terminal_buffer: Vec<String> = Vec::new();
    let mut converted_terminals: Vec<String> = Vec::new();
    let terminal_chars: Vec<char> = sentence.chars().collect();

    terminal_chars.iter()
        .enumerate()
        .for_each(|(i, &chr)| {
            if available_symbols.contains(&chr) {
                terminal_buffer.push(chr.to_string());
            }

            if terminal_buffer.len() == 2 ||
                (i == terminal_chars.len() - 1) {
                let concat_buffer: String = terminal_buffer.concat();
                if association_map.contains_key(&concat_buffer) {
                    converted_terminals.push(association_map.get(&concat_buffer).unwrap().to_string())
                }
                terminal_buffer.clear();
            }
        });

    return converted_terminals.concat();
}
